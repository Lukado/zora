module medicalc.zora {
    requires kotlin.stdlib;

    requires org.controlsfx.controls;
    requires org.kordamp.ikonli.javafx;
    requires org.jfxtras.styles.jmetro;
    requires ini4j;
    requires kotlin.stdlib.jdk8;
    requires javafx.fxml;
    requires java.prefs;
    requires org.jsoup;

    exports medicalc.zora;
}