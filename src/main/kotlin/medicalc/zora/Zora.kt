package medicalc.zora

import javafx.application.Application
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.image.Image
import javafx.stage.Stage

class Zora : Application() {
    override fun start(stage: Stage) {
        val fxmlLoader = FXMLLoader(Zora::class.java.getResource("zora.fxml"))

        val scene = Scene(fxmlLoader.load())
        jMetro.scene = scene
        val ctrl = fxmlLoader.getController<ZoraController>()

        stage.centerOnScreen()
        stage.x = pref.getDouble(ZoraPrefs.window_position_x.toString(), 1.0)
        stage.y = pref.getDouble(ZoraPrefs.window_position_y.toString(), 1.0)
        stage.width = pref.getDouble(ZoraPrefs.window_width.toString(), 1.0)
        stage.height = pref.getDouble(ZoraPrefs.window_height.toString(), 1.0)
        stage.minWidth = ctrl.mainStage.minWidth + 40
        stage.minHeight = ctrl.mainStage.minHeight + 40
        stage.title = "Zora - Medicalc configurator"
        stage.scene = scene
        stage.icons.add(Image(Zora::class.java.getResourceAsStream("speedo.jpg")))
        stage.show()

        stage.setOnCloseRequest {
            ctrl.onExit()
            pref.putDouble(ZoraPrefs.window_position_x.toString(), stage.x)
            pref.putDouble(ZoraPrefs.window_position_y.toString(), stage.y)
            pref.putDouble(ZoraPrefs.window_width.toString(), stage.width)
            pref.putDouble(ZoraPrefs.window_height.toString(), stage.height)
        }
    }
}
fun main() = Application.launch(Zora::class.java)
