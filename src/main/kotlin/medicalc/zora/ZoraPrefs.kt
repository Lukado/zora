package medicalc.zora

import jfxtras.styles.jmetro.JMetro
import jfxtras.styles.jmetro.Style
import java.util.prefs.Preferences

const val CONFIG_NAME = "Zora"
val pref: Preferences = Preferences.userRoot().node(CONFIG_NAME)
var jMetro = JMetro(if (pref.get("MC_WINDOW_STYLE", Style.LIGHT.name) == Style.LIGHT.name) Style.LIGHT else Style.DARK)
enum class ZoraPrefs {
    window_position_x,
    window_position_y,
    window_width,
    window_height,
    window_style,
    ini_paths,
    servers, // format - name1;name2;name3
    default_servers,
    logins, // format - name1|pass1;name2|pass2;
    default_logins,
    autoruns, // format - 1|1|||1||||1;2|2|2|2|2||2|2|2
    server_sel_row,
    login_sel_row,
    folder_sel_row,
    page_index,
    autoconnect
}