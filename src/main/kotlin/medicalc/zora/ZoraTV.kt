package medicalc.zora

class ZoraTV {
    data class ServerView(var serverName: String = "", var changed: Boolean = false)
    data class LoginView(var username: String = "", var password: String = "", var changed: Boolean = false)
    data class AutorunView(var klinOdd: String = "", var prac: String = "", var pokl: String = "", var primaAkce: String = "", var pacId: String = "",
                           var kluId: String = "", var hospStaId: String = "", var kodTypu: String = "", var treeItem: String = "", var changed: Boolean = false)
    data class FolderView(var folder: String = "")
}