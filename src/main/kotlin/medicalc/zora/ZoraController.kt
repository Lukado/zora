package medicalc.zora

import javafx.application.Platform
import javafx.event.ActionEvent
import javafx.event.EventHandler
import javafx.fxml.FXMLLoader
import javafx.scene.Scene
import javafx.scene.control.*
import javafx.scene.control.cell.PropertyValueFactory
import javafx.scene.input.KeyEvent
import javafx.scene.input.MouseEvent
import javafx.scene.layout.BorderPane
import javafx.scene.layout.GridPane
import javafx.stage.FileChooser
import javafx.stage.FileChooser.ExtensionFilter
import javafx.stage.Modality
import javafx.stage.Stage
import jfxtras.styles.jmetro.JMetroStyleClass
import jfxtras.styles.jmetro.Style
import org.controlsfx.control.NotificationPane
import org.controlsfx.control.SearchableComboBox
import org.controlsfx.control.ToggleSwitch
import org.controlsfx.control.tableview2.cell.TextField2TableCell
import java.io.File
import java.util.concurrent.TimeUnit
import java.util.function.UnaryOperator


@Suppress("UNUSED_PARAMETER", "SpellCheckingInspection")
class ZoraController {
    lateinit var btnUserSettings: Button
    lateinit var btnUserDefaults: Button
    lateinit var btnServerSettings: Button
    lateinit var btnServerDefaults: Button
    lateinit var colFolder: TableColumn<ZoraTV.FolderView, String>
    lateinit var tblFolders: TableView<ZoraTV.FolderView>
    lateinit var pnlPages: BorderPane
    lateinit var tglAC: ToggleSwitch
    lateinit var pnlGrid: GridPane
    lateinit var edtTreeItem: TextField
    lateinit var edtKodTypu: TextField
    lateinit var edtHospStaId: TextField
    lateinit var edtKluId: TextField
    lateinit var edtPacId: TextField
    lateinit var edtPrimaAkce: TextField
    lateinit var edtPokl: TextField
    lateinit var edtPrac: TextField
    lateinit var edtKlinOdd: TextField
    lateinit var colServername: TableColumn<ZoraTV.ServerView, String>
    lateinit var colPassword: TableColumn<ZoraTV.LoginView, String>
    lateinit var colUsername: TableColumn<ZoraTV.LoginView, String>
    lateinit var tblServers: TableView<ZoraTV.ServerView>
    lateinit var tblLogins: TableView<ZoraTV.LoginView>
    lateinit var cmbPrimaAkce: SearchableComboBox<String>
    lateinit var cmbTreeItem: SearchableComboBox<String>
    lateinit var pgcAutorun: Pagination
    lateinit var btnServerAdd: Button
    lateinit var btnServerRemove: Button
    lateinit var btnUserAdd: Button
    lateinit var btnUserRemove: Button
    lateinit var btnLoadConfig: Button
    lateinit var lblFilepath: Label
    lateinit var lblErr: Label
    lateinit var mainStage: BorderPane
    lateinit var btnLaunchMedicalc: Button
    lateinit var cfgReader: IniReader
    var autorunLists: ArrayList<ZoraTV.AutorunView> = ArrayList()

    fun initialize() {
        mainStage.styleClass.add(JMetroStyleClass.BACKGROUND)
        tblLogins.styleClass.add(JMetroStyleClass.ALTERNATING_ROW_COLORS)
        tblServers.styleClass.add(JMetroStyleClass.ALTERNATING_ROW_COLORS)
        tblFolders.styleClass.add(JMetroStyleClass.ALTERNATING_ROW_COLORS)

        when (pref.get(ZoraPrefs.window_style.name, ""))
        {
            Style.DARK.name -> jMetro.style = Style.DARK
            Style.LIGHT.name -> jMetro.style = Style.LIGHT
            "" -> jMetro.style = Style.LIGHT
        }

        cmbTreeItem.isVisible = false
        cmbPrimaAkce.isVisible = false

        for (i in 1..pgcAutorun.pageCount) {
            autorunLists.add(ZoraTV.AutorunView())
        }

        cfgReader = IniReader(this)

        createTableSettings()
        createAutorunSettings()

        pgcAutorun.currentPageIndexProperty().addListener { _ -> tableChangeListener() }
        pgcAutorun.setPageFactory { pageIndex -> createPage(pageIndex) }

    }

    private fun tableChangeListener() {
        cfgReader.writeIniChanges()
    }

    private fun folderChangeListener() {
        if (tblFolders.selectionModel.selectedIndex >= 0)
            cfgReader.folderChange()
    }

    private fun createTableSettings() {
        colServername.cellValueFactory = PropertyValueFactory("serverName")
        colUsername.cellValueFactory = PropertyValueFactory("username")
        colPassword.cellValueFactory = PropertyValueFactory("password")
        colFolder.cellValueFactory = PropertyValueFactory("folder")

        tblServers.selectionModel.selectedItemProperty().addListener { _, _, _ -> tableChangeListener() }
        tblLogins.selectionModel.selectedItemProperty().addListener { _, _, _ -> tableChangeListener() }
        tblFolders.selectionModel.selectedItemProperty().addListener { _, _, _ -> folderChangeListener() }

        tblServers.setOnScroll {
            if (it.eventType.name == "SCROLL") {
                if (it.deltaY < 0) tblServers.selectionModel.selectNext() else tblServers.selectionModel.selectPrevious()
            }
        }
        tblLogins.setOnScroll {
            if (it.eventType.name == "SCROLL") {
                if (it.deltaY < 0) tblLogins.selectionModel.selectNext() else tblLogins.selectionModel.selectPrevious()
            }
        }
        tblFolders.setOnScroll {
            if (it.eventType.name == "SCROLL") {
                if (it.deltaY < 0) tblFolders.selectionModel.selectNext() else tblFolders.selectionModel.selectPrevious()
            }
        }

        // SERVERNAME
        colServername.cellFactory = TextField2TableCell.forTableColumn()
        colServername.onEditCommit = EventHandler<TableColumn.CellEditEvent<ZoraTV.ServerView, String>> {
            if (it.newValue.isEmpty())
                it.tableView.items.remove((it.tableView.items[it.tablePosition.row]))
            else if (it.rowValue != null) {
                it.rowValue.serverName = it.newValue
                cfgReader.currServer.changed = true
                cfgReader.writeIniChanges()
                it.tableView.refresh()
            }
        }
        colServername.onEditCancel = EventHandler<TableColumn.CellEditEvent<ZoraTV.ServerView, String>> {
            if (it.tableView.items.count() != 1) {
                it.tableView.items.removeAll { item -> item.serverName.isEmpty() }
            }
            cfgReader.writeIniChanges()
        }

        // USERNAME
        colUsername.cellFactory = TextField2TableCell.forTableColumn()
        colUsername.onEditCommit = EventHandler<TableColumn.CellEditEvent<ZoraTV.LoginView, String>> {
            if (it.newValue.isEmpty() && it.rowValue.password.isEmpty())
                it.tableView.items.remove((it.tableView.items[it.tablePosition.row]))
            else  if (it.rowValue != null) {
                it.rowValue.username = it.newValue
                cfgReader.currLogin.changed = true
                if (it.rowValue.password.isEmpty())
                    it.rowValue.password = it.newValue.lowercase()
                cfgReader.writeIniChanges()
                it.tableView.refresh()
            }
        }
        colUsername.onEditCancel = EventHandler<TableColumn.CellEditEvent<ZoraTV.LoginView, String>> {
            if (it.tableView.items.count() != 1) {
                it.tableView.items.removeAll { item -> item.username.isEmpty() && item.password.isEmpty() }
            }
        }

        // PASSWORD
        colPassword.cellFactory = TextField2TableCell.forTableColumn()
        colPassword.onEditCommit = EventHandler<TableColumn.CellEditEvent<ZoraTV.LoginView, String>> {
            if (it.newValue.isEmpty() && it.rowValue.username.isEmpty())
                it.tableView.items.remove((it.tableView.items[it.tablePosition.row]))
            else if (it.rowValue != null) {
                it.rowValue.password = it.newValue
                cfgReader.currLogin.changed = true
                cfgReader.writeIniChanges()
                it.tableView.refresh()
            }
        }
        colPassword.onEditCancel = EventHandler<TableColumn.CellEditEvent<ZoraTV.LoginView, String>> {
            if (it.tableView.items.count() != 1) {
                it.tableView.items.removeAll { item -> item.username.isEmpty() && item.password.isEmpty() }
            }
        }

        // FOLDER

    }

    private fun createAutorunSettings() {
        edtKlinOdd.textFormatter = numericFormatter()
        edtPrac.textFormatter = numericFormatter()
        edtPokl.textFormatter = numericFormatter()
        edtKluId.textFormatter = numericFormatter()
        edtPacId.textFormatter = numericFormatter()
        edtHospStaId.textFormatter = numericFormatter()
        edtKodTypu.textFormatter = numericFormatter()
    }

    private fun numericFormatter(): TextFormatter<String> {
        return TextFormatter<String>(UnaryOperator {
            if (it.text.trim().matches("\\d*".toRegex())) {
                it.text = it.text.trim()
                return@UnaryOperator it
            }
            return@UnaryOperator null
        })
    }

    fun onLaunchMedicalc(actionEvent: ActionEvent) {
        "cmd /C \"start Medicalc4.exe\"".runCommand(File(lblFilepath.text.substringBeforeLast("\\")))
    }

    private fun String.runCommand(workingDir: File) {
            ProcessBuilder(*split(" ").toTypedArray())
                .directory(workingDir)
                .redirectOutput(ProcessBuilder.Redirect.INHERIT)
                .redirectError(ProcessBuilder.Redirect.PIPE)
                .start()
                .waitFor(10, TimeUnit.SECONDS)
    }

    fun onLoadConfig(actionEvent: ActionEvent) {
        val stage = mainStage.scene.window as Stage
        val fileChooser = FileChooser()
        fileChooser.title = "Load Medicalc4.ini file"
        fileChooser.initialDirectory = if (cfgReader.file.exists()) cfgReader.file.parentFile else File(System.getProperty("user.home"))
        fileChooser.extensionFilters.add(ExtensionFilter("Medicalc ini", "Medicalc4.ini"))
        val file = fileChooser.showOpenDialog(stage)
        if (file != null) {
            lblFilepath.text = file.path
            cfgReader.loadIni(file)
        }

    }

    fun onServerAdd(actionEvent: ActionEvent) {
        tblServers.items.add(ZoraTV.ServerView(""))
        Platform.runLater { tblServers.edit(tblServers.items.count() - 1, colServername) }
    }

    fun onServerRemove(actionEvent: ActionEvent) {
        tblServers.items.remove(tblServers.selectionModel.selectedItem)
        cfgReader.writeIniChanges()
    }

    fun onUserAdd(actionEvent: ActionEvent) {
        tblLogins.items.add(ZoraTV.LoginView("",""))
        Platform.runLater { tblLogins.edit(tblLogins.items.count() - 1, colUsername) }
    }

    fun onUserRemove(actionEvent: ActionEvent) {
        tblLogins.items.remove(tblLogins.selectionModel.selectedItem)
        cfgReader.writeIniChanges()
    }

    fun onSwapTheme(actionEvent: ActionEvent) {
        if (jMetro.style == Style.LIGHT)
            jMetro.style = Style.DARK
        else
            jMetro.style = Style.LIGHT

        pref.put(ZoraPrefs.window_style.name, jMetro.style.name)
    }

    fun onExit() {}

    fun createPage(pageIndex: Int) : NotificationPane {
        edtKlinOdd.text = autorunLists[pageIndex].klinOdd
        edtPrac.text = autorunLists[pageIndex].prac
        edtPokl.text = autorunLists[pageIndex].pokl
        edtPrimaAkce.text = autorunLists[pageIndex].primaAkce
        edtPacId.text = autorunLists[pageIndex].pacId
        edtKluId.text = autorunLists[pageIndex].kluId
        edtHospStaId.text = autorunLists[pageIndex].hospStaId
        edtKodTypu.text = autorunLists[pageIndex].kodTypu
        edtTreeItem.text = autorunLists[pageIndex].treeItem

        return NotificationPane()
    }

    fun onAutoconnectToggle(mouseEvent: MouseEvent) {
        cfgReader.writeIniChanges()
    }

    fun onAutorunChanged(actionEvent: KeyEvent) {
        val idName = when (actionEvent.source) {
            is TextField -> (actionEvent.source as TextField).id
            is ComboBox<*> -> (actionEvent.source as ComboBox<*>).id
            else -> { "Unknown source" }
        }

        val text = when (actionEvent.source) {
            is TextField -> (actionEvent.source as TextField).text
            is ComboBox<*> -> (actionEvent.source as ComboBox<*>).selectionModel.selectedItem.toString()
            else -> { "Unknown source" }
        }

        val prevText = autorunLists[pgcAutorun.currentPageIndex].toString()
        when {
            idName.contains(ZoraTV.AutorunView::klinOdd.name, true) -> autorunLists[pgcAutorun.currentPageIndex].klinOdd = text
            idName.contains(ZoraTV.AutorunView::prac.name, true) -> autorunLists[pgcAutorun.currentPageIndex].prac = text
            idName.contains(ZoraTV.AutorunView::pokl.name, true) -> autorunLists[pgcAutorun.currentPageIndex].pokl = text
            idName.contains(ZoraTV.AutorunView::primaAkce.name, true) -> autorunLists[pgcAutorun.currentPageIndex].primaAkce = text
            idName.contains(ZoraTV.AutorunView::pacId.name, true) -> autorunLists[pgcAutorun.currentPageIndex].pacId = text
            idName.contains(ZoraTV.AutorunView::kluId.name, true) -> autorunLists[pgcAutorun.currentPageIndex].kluId = text
            idName.contains(ZoraTV.AutorunView::hospStaId.name, true) -> autorunLists[pgcAutorun.currentPageIndex].hospStaId = text
            idName.contains(ZoraTV.AutorunView::kodTypu.name, true) -> autorunLists[pgcAutorun.currentPageIndex].kodTypu = text
            idName.contains(ZoraTV.AutorunView::treeItem.name, true) -> autorunLists[pgcAutorun.currentPageIndex].treeItem = text
        }
        cfgReader.currAutorun.changed = prevText != autorunLists[pgcAutorun.currentPageIndex].toString()

        // println("$idName = $text")
        cfgReader.writeIniChanges()
    }

    fun onServerDefaults(actionEvent: ActionEvent) {
        val serverList = pref.get(ZoraPrefs.default_servers.name, "")
        if (serverList.isNotEmpty())
            serverList.split(";").map { ZoraTV.ServerView(it) }.forEach { if (!tblServers.items.contains(it)) tblServers.items.add(it) }
    }

    fun onServerSettings(actionEvent: ActionEvent) {
        val dialog = Stage()
        val fxmlLoader = FXMLLoader(Zora::class.java.getResource("DefaultList.fxml"))
        val scene = Scene(fxmlLoader.load())
        jMetro.scene = scene

        dialog.initOwner(mainStage.scene.window)
        dialog.initModality(Modality.APPLICATION_MODAL)
        dialog.scene = scene
        dialog.width = 200.0
        dialog.height = 400.0
        dialog.centerOnScreen()
        dialog.showAndWait()
    }

    fun onUserDefaults(actionEvent: ActionEvent) {
        val loginList = pref.get(ZoraPrefs.default_logins.name, "")
        if (loginList.isNotEmpty())
            loginList.split(";").map { ZoraTV.LoginView(it.split("|")[0], it.split("|")[1]) }.forEach { if (!tblLogins.items.contains(it)) tblLogins.items.add(it) }
    }

    fun onUserSettings(actionEvent: ActionEvent) {
        val dialog = Stage()
        val fxmlLoader = FXMLLoader(Zora::class.java.getResource("DefaultList.fxml"))
        val scene = Scene(fxmlLoader.load())
        jMetro.scene = scene

        dialog.initOwner(mainStage.scene.window)
        dialog.initModality(Modality.APPLICATION_MODAL)
        dialog.scene = scene
        dialog.width = 200.0
        dialog.height = 400.0
        dialog.centerOnScreen()
        dialog.showAndWait()
    }



}
