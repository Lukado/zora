package medicalc.zora

import javafx.scene.paint.Paint
import org.ini4j.Wini
import java.io.File
import java.nio.charset.StandardCharsets

class IniReader(var ctrl: ZoraController) {
    private lateinit var ini: Wini

    var file: File
    var currAC: Boolean = false
    var currFolder: ZoraTV.FolderView = ZoraTV.FolderView()
    var currServer: ZoraTV.ServerView = ZoraTV.ServerView()
    var currLogin: ZoraTV.LoginView = ZoraTV.LoginView()
    var currAutorun: ZoraTV.AutorunView = ZoraTV.AutorunView()
    var isLoading: Boolean = false
    var primaAkce: HashMap<String, String> = HashMap()

    init {
        file = File(pref.get(ZoraPrefs.folder_sel_row.name, ""))
        loadFolders(file)
        loadFile(file)
    }

    fun loadIni(file: File)
    {
        loadFolders(file, false)
        loadFile(file, false)
    }

    private fun loadFolders(file: File, isStartup: Boolean = true)
    {
        if (file.exists()) {
            try {
                if (isStartup) {
                    pref.put(ZoraPrefs.ini_paths.name, pref.get(ZoraPrefs.ini_paths.name, "").split(";").filter { File(it).exists() }.joinToString(";"))
                    pref.get(ZoraPrefs.ini_paths.name, "").split(";").forEach {
                        if (!it.endsWith("Release\\Medicalc4.ini"))
                            ctrl.tblFolders.items.add(ZoraTV.FolderView(it.substringBeforeLast("\\").substringAfterLast("\\")))
                        else
                            ctrl.tblFolders.items.add(ZoraTV.FolderView(it.substringBeforeLast("\\").substringBeforeLast("\\").substringAfterLast("\\")))
                    }
                }
                else {
                    val parentFolder =
                        if (file.path.endsWith("Release\\Medicalc4.ini"))
                            file.path.substringBeforeLast("\\").substringBeforeLast("\\").substringBeforeLast("\\")
                        else
                            file.path.substringBeforeLast("\\")

                    var iniPaths = pref.get(ZoraPrefs.ini_paths.name, "").split(";").filter { File(it).exists() }.joinToString(";")
                    if (!iniPaths.contains(file.absolutePath))
                        iniPaths += if (iniPaths.isEmpty()) file.absolutePath else ";" + file.absolutePath
                    if (!file.path.endsWith("Release\\Medicalc4.ini") && !ctrl.tblFolders.items.contains(ZoraTV.FolderView(file.path.substringBeforeLast("\\").substringAfterLast("\\"))))
                        ctrl.tblFolders.items.add(ZoraTV.FolderView(file.path.substringBeforeLast("\\").substringAfterLast("\\")))

                    File(parentFolder).walkTopDown().filter { it.path.endsWith("Release\\Medicalc4.ini") }.forEach {
                        if (!ctrl.tblFolders.items.contains(ZoraTV.FolderView(it.path.substringBeforeLast("\\").substringBeforeLast("\\").substringAfterLast("\\"))))
                            ctrl.tblFolders.items.add(ZoraTV.FolderView(it.path.substringBeforeLast("\\").substringBeforeLast("\\").substringAfterLast("\\")))
                        if (!iniPaths.contains(it.absolutePath))
                            iniPaths += if (iniPaths.isEmpty()) it.absolutePath else ";" + it.absolutePath
                    }

                    pref.put(ZoraPrefs.ini_paths.name, iniPaths)
                }
            } catch (_: Exception) {
                ctrl.lblErr.text += if (ctrl.lblErr.text.isEmpty()) "Could not load section: Folders" else "Folders"
            }

            if (ctrl.tblFolders.items.isNotEmpty()) {
                val folder =
                    if (!file.path.endsWith("Release\\Medicalc4.ini"))
                        ZoraTV.FolderView(file.path.substringBeforeLast("\\").substringAfterLast("\\"))
                    else
                        ZoraTV.FolderView(file.path.substringBeforeLast("\\").substringBeforeLast("\\").substringAfterLast("\\"))
                if (folder.folder.isNotEmpty())
                    ctrl.tblFolders.selectionModel.select(folder)
                else
                    ctrl.tblFolders.selectionModel.select(0)

                currFolder = ctrl.tblFolders.selectionModel.selectedItem
                pref.put(ZoraPrefs.folder_sel_row.name, pref.get(ZoraPrefs.ini_paths.name, "").split(";").first { it.contains(ctrl.tblFolders.selectionModel.selectedItem.folder) }.toString())
            }
        }
    }

    private fun loadFile(file: File, isStartup: Boolean = true) {
        ctrl.lblErr.text = ""
        ctrl.lblErr.textFill = Paint.valueOf("RED")
        if (file.exists()) {
            this.file = file
            ini = Wini(this.file)

            isLoading = true
            readIni(isStartup)
            isLoading = false

            ctrl.lblFilepath.text = this.file.path
            ctrl.pnlGrid.isDisable = false
            ctrl.pnlPages.isDisable = false
            ctrl.btnLaunchMedicalc.isDisable = false

            /* TODO: parsování ANSI souborů nebere diakritiku prostě
            try {
                val messageFile = File(Path(file.absolutePath + "\\..\\..\\Common\\uDefineMessages.h").normalize().toString())
                val treeItemsFile = File(Path(file.absolutePath + "\\..\\..\\Common\\uTreeMenuStd.cpp").normalize().toString())
                if (messageFile.exists()) {
                    ctrl.cmbPrimaAkce.isVisible = true
                    ctrl.edtPrimaAkce.isVisible = false
                    parseMessages(messageFile)
                }
                if (treeItemsFile.exists()) {
                    ctrl.cmbTreeItem.isVisible = true
                    ctrl.edtTreeItem.isVisible = false
                    parseTreeItems(treeItemsFile)
                }
            }catch (_: Exception) {}
            */
            writeIniChanges()
        }
        else {
            ctrl.lblFilepath.text = ""
            ctrl.pnlGrid.isDisable = true
            ctrl.pnlPages.isDisable = true
            ctrl.btnLaunchMedicalc.isDisable = true
        }
    }

    private fun parseMessages(messageFile: File) {
        var enumFound = false
        messageFile.forEachLine {
            enumFound = if (!enumFound) it.contains("enum TPrimaAkce") else true
            if (enumFound && it.trim().startsWith("pa"))
            {
                if (it.split("=").first().trim() != "paOpravHospit") {
                    ctrl.cmbPrimaAkce.items.add(it.split("=").first().trim())
                    primaAkce[it.split("=").first().trim()] = it.split("=")[1].substringBefore("//").substringBefore(",").trim()
                }
            }
            else if (enumFound && it.trim().contains("}")) {
                enumFound = false
            }
        }
    }

    private fun parseTreeItems(treeItemsFile: File) {
        treeItemsFile.bufferedReader(StandardCharsets.ISO_8859_1).forEachLine {
            if (it.trim().contains("CreateItem"))
            {
                ctrl.cmbTreeItem.items.add(it.substringAfter(",").substringBefore(",").trim())
            }
        }
    }

    private fun readIni(isStartup: Boolean)
    {
        readServers(isStartup)
        readLogins(isStartup)
        readAutoruns()
    }

    fun writeIniChanges() {
        if (isLoading)
            return
        // write Servers
        if (currServer != ctrl.tblServers.selectionModel.selectedItem || currServer.changed) {
            currServer.changed = false
            currServer = ctrl.tblServers.selectionModel.selectedItem ?: ZoraTV.ServerView()
            ini.put("Database", "ServerName", currServer.serverName)
            writeServers()
        }
        // write Logins
        if (currLogin != ctrl.tblLogins.selectionModel.selectedItem || currLogin.changed) {
            currLogin.changed = false
            currLogin = ctrl.tblLogins.selectionModel.selectedItem ?: ZoraTV.LoginView()
            ini.put("Debug", "UserName", currLogin.username)
            ini.put("Debug", "Password", currLogin.password)
            writeLogins()
        }
        // write Autoruns
        if (currAutorun != ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex] || currAutorun.changed) {
            currAutorun = ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex]
            currAutorun.changed = false
            ini.put("AutoRun", "KlinOddCislo", currAutorun.klinOdd)
            ini.put("AutoRun", "PracovisteCislo", currAutorun.prac)
            ini.put("AutoRun", "PokladnaCislo", currAutorun.pokl)
            ini.put("AutoRun", "PrimaAkce", currAutorun.primaAkce)
            ini.put("AutoRun", "PacientID", currAutorun.pacId)
            ini.put("AutoRun", "KlinUdalID", currAutorun.kluId)
            ini.put("AutoRun", "HospStaID", currAutorun.hospStaId)
            ini.put("AutoRun", "KodTypu", currAutorun.kodTypu)
            ini.put("AutoRun", "TreeItem", currAutorun.treeItem)
            writeAutoruns()
        }

        if (currAC != ctrl.tglAC.isSelected) {
            currAC = ctrl.tglAC.isSelected
            pref.putBoolean(ZoraPrefs.autoconnect.name, currAC)
            ini.put("Debug", "AutoConnect", currAC)
        }

        ini.store()
    }

    private fun writeServers() {
        pref.putInt(ZoraPrefs.server_sel_row.name, ctrl.tblServers.selectionModel.selectedIndex.coerceAtLeast(0))
        val servers = ctrl.tblServers.items.joinToString(";") { it.serverName }
        if (servers != pref.get(ZoraPrefs.servers.name, ""))
            pref.put(ZoraPrefs.servers.name, servers)
    }

    private fun writeLogins() {
        pref.putInt(ZoraPrefs.login_sel_row.name, ctrl.tblLogins.selectionModel.selectedIndex.coerceAtLeast(0))
        val logins = ctrl.tblLogins.items.joinToString(";") { it.username + "|" + it.password }
        if (logins != pref.get(ZoraPrefs.servers.name, ""))
            pref.put(ZoraPrefs.logins.name, logins)
    }

    private fun writeAutoruns() {
        pref.putInt(ZoraPrefs.page_index.name, ctrl.pgcAutorun.currentPageIndex)
        val autoruns = ctrl.autorunLists.joinToString(";") {
            it.klinOdd + "|" + it.prac + "|" + it.pokl + "|" + it.primaAkce + "|" + it.pacId + "|" + it.kluId + "|" + it.hospStaId + "|" + it.kodTypu + "|" + it.treeItem
        }
        if (autoruns != pref.get(ZoraPrefs.autoruns.name, ""))
            pref.put(ZoraPrefs.autoruns.name, autoruns)
    }

    private fun readServers(isStartup: Boolean)
    {
        val serverList = pref.get(ZoraPrefs.servers.name, "")
        if (serverList.isNotEmpty()) {
            ctrl.tblServers.items.clear()
            ctrl.tblServers.items.addAll(serverList.split(";").map { ZoraTV.ServerView(it) })
        }

        try {
            if (!isStartup && !ctrl.tblServers.items.contains(ZoraTV.ServerView(ini.get("Database", "ServerName"))))
                ctrl.tblServers.items.add(ZoraTV.ServerView(ini.get("Database", "ServerName")))
        } catch (_: Exception){
            ctrl.lblErr.text += if (ctrl.lblErr.text.isEmpty()) "Could not load section: Database" else "Database"
        }

        if (ctrl.tblServers.items.isNotEmpty()) {
            currServer = ctrl.tblServers.items[pref.getInt(ZoraPrefs.server_sel_row.name, 0)]
            ctrl.tblServers.selectionModel.select(pref.getInt(ZoraPrefs.server_sel_row.name, 0))
        }
    }

    private fun readLogins(isStartup: Boolean) {
        val loginList = pref.get(ZoraPrefs.logins.name, "")
        currAC = pref.getBoolean(ZoraPrefs.autoconnect.name, false)
        if (loginList.isNotEmpty()) {
            ctrl.tblLogins.items.clear()
            ctrl.tblLogins.items.addAll(loginList.split(";").map { ZoraTV.LoginView(it.split("|")[0], it.split("|")[1]) })
        }

        try {
            val loginName = ini.get("Debug", "UserName")
            val loginPassword = ini.get("Debug", "Password")
            if (!isStartup && !ctrl.tblLogins.items.contains(ZoraTV.LoginView(loginName, loginPassword)))
                ctrl.tblLogins.items.add(ZoraTV.LoginView(loginName, loginPassword))

            val loginAutoconnect = ini.get("Debug", "AutoConnect")
            currAC = loginAutoconnect.toBoolean()
        } catch (_: Exception){
            ctrl.lblErr.text += if (ctrl.lblErr.text.isEmpty()) "Could not load section: Debug" else ", Debug"
        }

        ctrl.tglAC.isSelected = currAC
        if (ctrl.tblLogins.items.isNotEmpty()) {
            currLogin = ctrl.tblLogins.items[pref.getInt(ZoraPrefs.login_sel_row.name, 0)]
            ctrl.tblLogins.selectionModel.select(pref.getInt(ZoraPrefs.login_sel_row.name, 0))
        }
    }

    private fun readAutoruns() {
        val arList = pref.get(ZoraPrefs.autoruns.name, "")
        if (arList.isNotEmpty()) {
            arList.split(";").forEachIndexed { index, it ->
                ctrl.autorunLists[index].klinOdd = it.split("|")[0]
                ctrl.autorunLists[index].prac = it.split("|")[1]
                ctrl.autorunLists[index].pokl = it.split("|")[2]
                ctrl.autorunLists[index].primaAkce = it.split("|")[3]
                ctrl.autorunLists[index].pacId = it.split("|")[4]
                ctrl.autorunLists[index].kluId = it.split("|")[5]
                ctrl.autorunLists[index].hospStaId = it.split("|")[6]
                ctrl.autorunLists[index].kodTypu = it.split("|")[7]
                ctrl.autorunLists[index].treeItem = it.split("|")[8]
            }
        }
        else {
            try {
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].klinOdd = ini.get("AutoRun", "KlinOddCislo")
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].prac = ini.get("AutoRun", "PracovisteCislo")
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].pokl = ini.get("AutoRun", "PokladnaCislo")
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].primaAkce = ini.get("AutoRun", "PrimaAkce")
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].pacId = ini.get("AutoRun", "PacientID")
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].kluId = ini.get("AutoRun", "KlinUdalID")
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].hospStaId = ini.get("AutoRun", "HospStaID")
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].kodTypu = ini.get("AutoRun", "KodTypu")
                ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex].treeItem = ini.get("AutoRun", "TreeItem")

                ctrl.createPage(ctrl.pgcAutorun.currentPageIndex)
            } catch (_: Exception){
                ctrl.lblErr.text += if (ctrl.lblErr.text.isEmpty()) "Could not load section: AutoRun" else ", AutoRun"
            }
        }

        ctrl.pgcAutorun.currentPageIndex = pref.getInt(ZoraPrefs.page_index.name, 0)
        currAutorun = ctrl.autorunLists[ctrl.pgcAutorun.currentPageIndex]
    }

    fun folderChange() {
        if (currFolder != ctrl.tblFolders.selectionModel.selectedItem) {
            pref.get(ZoraPrefs.ini_paths.name, "").split(";").filter { !File(it).exists() }.forEach {
                val folderName =
                    if (it.endsWith("Release\\Medicalc4.ini"))
                        it.substringBeforeLast("\\").substringBeforeLast("\\").substringAfterLast("\\")
                    else
                        it.substringBeforeLast("\\").substringAfterLast("\\")
                ctrl.tblFolders.items.remove(ZoraTV.FolderView(folderName))
            }
            pref.put(ZoraPrefs.ini_paths.name, pref.get(ZoraPrefs.ini_paths.name, "").split(";").filter { File(it).exists() }.joinToString (";"))

            if (ctrl.tblFolders.selectionModel.selectedIndex == -1) {
                currFolder = ZoraTV.FolderView()
                loadFile(File(""))
            }
            else {
                currFolder = ctrl.tblFolders.selectionModel.selectedItem
                currServer.changed = true
                currLogin.changed = true
                currAutorun.changed = true
                loadFile(File(pref.get(ZoraPrefs.ini_paths.name, "").split(";")[ctrl.tblFolders.selectionModel.selectedIndex]))
            }

            pref.put(ZoraPrefs.folder_sel_row.name, pref.get(ZoraPrefs.ini_paths.name, "").split(";").first { it.contains(ctrl.tblFolders.selectionModel.selectedItem.folder) }.toString())
        }
    }
}